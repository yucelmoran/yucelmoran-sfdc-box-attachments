%dw 1.0
%output application/java
---
{
    "type": "error",
    "status": 409,
    "code": "item_name_in_use",
    "context_info": {
        "conflicts": {
            "type": "file",
            "id": "34311309605",
            "file_version": {
                "type": "file_version",
                "id": "33606532599",
                "sha1": "dad50de3cb336f66f96d548d0dfee0549c0c50e2"
            },
            "sequence_id": "0",
            "etag": "0",
            "sha1": "dad50de3cb336f66f96d548d0dfee0549c0c50e2",
            "name": "a0fD000000JREuLIAX-00PD000000KWR05MAH-IO - TEMPLATE - (STANDARD) May 2013 v2.4.pdf"
        }
    },
    "help_url": "http://developers.box.com/docs/#errors",
    "message": "Item with the same name already exists",
    "request_id": "18002017155c0e370dfd9f"
}