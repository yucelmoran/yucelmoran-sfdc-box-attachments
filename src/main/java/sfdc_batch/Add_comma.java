package sfdc_batch;


import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import com.opensymphony.util.TextUtils;
import java.util.ArrayList;

public class Add_comma implements Callable{
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		
		String jsonString = eventContext.getMessage().getPayloadAsString();
		
		
		//System.out.println("#### PAYLOAD: "+jsonString);
		
		String[] values = jsonString.split(",");
		ArrayList<String> arr = new ArrayList<>();
		
		//String nuevo = "";
		for(String s:values){
			//System.out.println("#### Nuevo valor: "+s);
			arr.add("'"+s.trim()+"'");
			//nuevo = String.join(",", s);
		}
		
		String res = TextUtils.join(",",arr);
		//String res = String.join(",", arr);
		
		String first = res.replace("[", "").trim();
		String second = first.replace("]", "").trim();
		String third = second.replace(".", "").trim();
		
		eventContext.getMessage().setInvocationProperty("comma_separated", third.trim());
		
		return eventContext.getMessage();
	}
}
